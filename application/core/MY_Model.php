<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

  public function is_login_parent($user_id, $user_regId){       
        $sql    = "select * from tb_user where ID_USER='".$user_id."' and user_regId='".$user_regId."'";
        $query  = $this->db->query($sql);
        if($query->num_rows()==1){
            return true;
        }else{
            return false;
        }
  }

  public function getAllUserNonOpsnallLoginParent(){
    $sql  = "select b.user_regId
            from tb_penyidik a, tb_user b
            where a.IS_OPSNAL=0
            and b.ID_USER=a.NRP
            and b.user_regId != ''";

    $data = $this->db->query($sql)->result();
    return $data; 
  }

	public function getAllUserReceiveNotofication($nrp) {
        $sql   = "SELECT c.NRP, c.NAMA_PENYIDIK, c.PANGKAT_PENYIDIK, c.IS_OPSNAL, d.tipe_user, d.USER_NAME, d.PASSWORD, d.user_regId
              FROM tb_penyidik c,
                   (SELECT NAMA_SATWIL,
                           NAMA_SATKER,
                           NAMA_UNIT,
                           NAMA_SUBDIT
                      FROM tb_penyidik a
                     WHERE NRP = '$nrp') AS b, tb_user d
             WHERE     c.NAMA_SATWIL = b.NAMA_SATWIL
                   AND c.NAMA_SATKER = b.NAMA_SATKER
                   AND c.NAMA_UNIT = b.NAMA_UNIT
                   AND c.NAMA_SUBDIT = b.NAMA_SUBDIT
                   and  c.nrp = d.id_user 
                   and d.user_regId !=''
                   and c.is_opsnal <> 1
                   union 
             SELECT c.NRP, c.NAMA_PENYIDIK, c.PANGKAT_PENYIDIK, c.IS_OPSNAL, d.tipe_user, d.USER_NAME, d.PASSWORD, d.user_regId
              FROM tb_penyidik c,
                   (SELECT NAMA_SATWIL,
                           NAMA_SATKER,
                           '' NAMA_UNIT,
                           NAMA_SUBDIT
                      FROM tb_penyidik a
                     WHERE NRP = '$nrp') AS b, tb_user d
             WHERE     c.NAMA_SATWIL = b.NAMA_SATWIL
                   AND c.NAMA_SATKER = b.NAMA_SATKER
                   AND c.NAMA_SUBDIT = b.NAMA_SUBDIT
                   and  c.nrp = d.id_user 
                   and d.TIPE_USER = 3
                   and d.user_regId !=''
                   and c.is_opsnal <> 1
                   union
             SELECT c.NRP, c.NAMA_PENYIDIK, c.PANGKAT_PENYIDIK, c.IS_OPSNAL, d.tipe_user, d.USER_NAME, d.PASSWORD, d.user_regId
              FROM tb_penyidik c,
                   (SELECT NAMA_SATWIL,
                           NAMA_SATKER,
                           '' NAMA_UNIT,
                           NAMA_SUBDIT
                      FROM tb_penyidik a
                     WHERE NRP = '$nrp') AS b, tb_user d
             WHERE     c.NAMA_SATWIL = b.NAMA_SATWIL
                   AND c.NAMA_SATKER = b.NAMA_SATKER
                   and  c.nrp = d.id_user 
                   and d.TIPE_USER = 2
                   and d.user_regId !=''
                   and c.is_opsnal <> 1";

        $data = $this->db->query($sql)->result();
        return $data;		
	}
}