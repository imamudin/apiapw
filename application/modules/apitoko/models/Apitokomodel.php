<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apitokomodel extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
    public function login($username, $password){
        $stat   = "select a.*, b.NAMA, c.user_regid from  tb_user_toko c, tb_toko a
                left join tb_pegawai b on b.NIK=a.KDSALES
                where c.ID_USER=a.KODE
                and binary c.USER_NAME='$username'
                and binary c.PASSWORD ='$password'                
                ";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function updateToken($username, $password, $token){
        $query = "update tb_user_toko set user_regid='$token' 
                    where binary user_name='$username' and binary password = '$password'";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function getUser($username, $password){
    //     $stat   = "select a.*, b.NAMA as NAMA_SALES, c.user_regid from  tb_user_toko c, tb_toko a
                // left join tb_pegawai b on b.NIK=a.KDSALES
    //             where c.ID_USER=a.KODE
    //             and binary c.USER_NAME='$username'
    //             and binary c.PASSWORD ='$password'";

        $stat   = "select a.*, b.NAMA as NAMA_SALES, c.user_regid, ifnull(TOTAL_PIUTANG, 0) TOTAL_PIUTANG, ifnull(PIUTANG, 0) TAGIHAN_TERDEKAT, TGL_JATUH_TEMPO
                    from  tb_user_toko c, tb_toko a
                    left join tb_pegawai b on b.NIK=a.KDSALES
                    left outer join 
                    (select b.KODE, sum(total) PIUTANG, 
                                            date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                                            from tb_order a, tb_toko b, tb_user_toko c
                                            where a.KODE_TOKO = b.KODE and is_approval = 1
                                            and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.USER_NAME = '$username'
                                            group by b.KODE
                                            union all
                                        select b.KODE,  sum(a.total) - sum(x.TOTAL) PIUTANG,
                                            date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                                            from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
                                            where a.KODE_TOKO = b.KODE  and is_approval = 1
                                            and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.USER_NAME = '$username'
                                            group by b.KODE
                                            order by TGL
                                            limit 0,1) X on a.KODE = X.KODE
                     left outer join 
                    (select KODE,  sum(PIUTANG) TOTAL_PIUTANG from (
                                        select b.KODE,  sum(total) PIUTANG
                                            from tb_order a, tb_toko b, tb_user_toko c
                                            where a.KODE_TOKO = b.KODE and is_approval = 1
                                            and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.USER_NAME = '$username'
                                            group by b.KODE
                                            union all
                                        select b.KODE,  sum(a.total) - sum(x.TOTAL) PIUTANG
                                            from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
                                            where a.KODE_TOKO = b.KODE  and is_approval = 1
                                            and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.USER_NAME = '$username'
                                            group by b.KODE) x
                                        group by KODE) Y on a.KODE = Y.KODE                      
                    where c.ID_USER=a.KODE
                    and binary c.USER_NAME='$username'
                    and binary c.PASSWORD ='$password'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getUserByToken($id_user, $user_token){
        $stat   = "select a.*, b.NAMA as NAMA_SALES, c.user_regid from  tb_user_toko c, tb_toko a
                left join tb_pegawai b on b.NIK=a.KDSALES
                where c.ID_USER=a.KODE
                and binary c.id_user='$id_user'
                and binary c.user_regid ='$user_token'";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public  function logout($id_user, $user_token){
        $sql    = "select * from tb_user_toko where binary id_user='".$id_user."' or binary user_regid='".$user_token."'";
        $data   = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            $data = $data[0];

            if($this->updateToken($data->USER_NAME, $data->PASSWORD, '')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function getPembayarans($KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = 25;
        }

        
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and TGL_KUITANSI BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT NAMA_PELANGGAN, ALAMAT_PELANGGAN, PEMBAYARAN, NO_KUITANSI, date_format(TGL_KUITANSI, '%d-%m-%Y') TGL_KUITANSI, 
                        NO_FAKTUR, date_format(TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, TOTAL TOTAL
                  FROM tb_pembayaran_toko a
                  where a.PELANGGAN = '$KODE'
                " . $sqladd . "
                 order by TGL_KUITANSI desc, NAMA_PELANGGAN
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }

    public function getPembayaran($kode){
        $stat   = "SELECT b.USER_REGID,
                       NAMA_PELANGGAN,
                       ALAMAT_PELANGGAN,
                       PEMBAYARAN,
                       NO_KUITANSI,
                       date_format(TGL_KUITANSI, '%d-%m-%Y') TGL_KUITANSI,
                       NO_FAKTUR,
                       date_format(TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR,
                       TOTAL
                  FROM tb_pembayaran_toko a, tb_user_toko b
                 WHERE a.PELANGGAN = '$kode' AND a.PELANGGAN = b.ID_USER
                 order by date_format (tgl_masuk_data, '%Y-%m-%d') desc
                 limit 0,1";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function getKunjungans($KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = 25;
        }

        //untuk menambahkan custom query
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and tgl_kunjungan BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT b.NIK, b.NAMA NAMA_PEGAWAI, c.KODE, c.NAMA NAMA_TOKO, date_format(tgl_kunjungan, '%d-%m-%Y') TGL_KUNJUNGAN, 
                TIME_FORMAT(JAM_MULAI, '%H:%i') JAM_MULAI, TIME_FORMAT(JAM_SELESAI, '%H:%i') JAM_SELESAI, 
                concat(TIME_FORMAT(TIMEDIFF(JAM_SELESAI,JAM_MULAI), '%H')*60 + TIME_FORMAT(TIMEDIFF(JAM_SELESAI,JAM_MULAI), '%i'), ' menit') DURASI 
                FROM tb_kunjungan a, tb_pegawai b, tb_toko c
                where a.nik = b.nik and a.kode = c.kode
                and b.nik = c.kdsales
                and c.KODE = '$KODE'
                " . $sqladd . "
                 order by tgl_kunjungan desc, JAM_MULAI desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getOrders($KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and tgl_faktur BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT date_format(tgl_faktur, '%d-%m-%Y') TGL_FAKTUR, a.ID_ORDER, a.NO_FAKTUR, c.KODE, c.NAMA NAMA_TOKO, 
                a.TOTAL,  date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO,
                date_format(TGL_KIRIM, '%d-%m-%Y') TGL_KIRIM,  b.NAMA NAMA_PEGAWAI, d.NAMA NAMA_GUDANG, a.ID_ORDER
                FROM tb_order a,  tb_pegawai b, tb_toko c, tb_gudang d
                where a.nik = b.nik and a.kode_toko = c.kode and a.KODE_GUDANG = d.KODE
                and b.nik = c.kdsales and is_approval = 1
                and c.KODE = '$KODE'
                " . $sqladd . "
                 order by tgl_faktur desc, c.NAMA desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getDetailOrders($id_order){
        $sql = "select a.NO_FAKTUR, date_format(tgl_faktur, '%d-%m-%Y') TGL_FAKTUR, 
                        c.NAMA_PRODUK, b.JUMLAH, b.HARGA, b.JUMLAH_HARGA
                from tb_order a, tb_order_detail b, tb_produk c
                where a.id_order = b.id_order and b.id_produk = c.id_produk and is_approval = 1
                and b.id_order = " . $this->db->escape($id_order) . "
                order by b.id_produk desc";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getDataPelanggan($KODE, $user_regid){
        // $stat   = "select a.*, b.NAMA as NAMA_SALES, c.user_regid from  tb_user_toko c, tb_toko a
        //         left join tb_pegawai b on b.NIK=a.KDSALES
        //         where c.ID_USER=a.KODE
        //         and binary c.ID_USER='$KODE'
        //         and binary c.user_regid ='$user_regid'";
//        $stat   = "select a.*, b.NAMA as NAMA_SALES, c.user_regid, ifnull(TOTAL_PIUTANG, 0) TOTAL_PIUTANG, ifnull(PIUTANG, 0) TAGIHAN_TERDEKAT, TGL_JATUH_TEMPO
//                    from  tb_user_toko c, tb_toko a
//                    left join tb_pegawai b on b.NIK=a.KDSALES
//                    left outer join 
//                    (select b.KODE, sum(total) PIUTANG, 
//                                            date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                                            from tb_order a, tb_toko b, tb_user_toko c
//                                            where a.KODE_TOKO = b.KODE and is_approval = 1
//                                            and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                            group by b.KODE
//                                            union all
//                                        select b.KODE,  sum(a.total) - sum(x.TOTAL) PIUTANG,
//                                            date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                                            from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
//                                            where a.KODE_TOKO = b.KODE  and is_approval = 1
//                                            and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                            group by b.KODE
//                                            order by TGL
//                                            limit 0,1) X on a.KODE = X.KODE
//                     left outer join 
//                    (select KODE,  sum(PIUTANG) TOTAL_PIUTANG from (
//                                        select b.KODE,  sum(total) PIUTANG
//                                            from tb_order a, tb_toko b, tb_user_toko c
//                                            where a.KODE_TOKO = b.KODE and is_approval = 1
//                                            and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                            group by b.KODE
//                                            union all
//                                        select b.KODE,  sum(a.total) - sum(x.TOTAL) PIUTANG
//                                            from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
//                                            where a.KODE_TOKO = b.KODE  and is_approval = 1
//                                            and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                            group by b.KODE) x
//                                        group by KODE) Y on a.KODE = Y.KODE                      
//                    where c.ID_USER=a.KODE
//                    and binary c.ID_USER='$KODE'
//                and binary c.user_regid ='$user_regid'";
        
        $stat   =  "select a.*, b.NAMA as NAMA_SALES, c.user_regid, ifnull(TOTAL_PIUTANG, 0) TOTAL_PIUTANG, ifnull(PIUTANG, 0) TAGIHAN_TERDEKAT, TGL_JATUH_TEMPO
                    from  tb_user_toko c, tb_toko a
                    left join tb_pegawai b on b.NIK=a.KDSALES
                    left outer join 
                    (select b.KODE, sum(jumlah) PIUTANG, 
                                            date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                                            from tb_penjualan a, tb_toko b, tb_user_toko c
                                            where a.K_PLG = b.KODE 
                                            and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                            group by b.KODE
                                            union all
                                        select b.KODE,  sum(a.jumlah) - sum(x.TOTAL) PIUTANG,
                                            date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                                            from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
                                            where a.K_PLG = b.KODE
                                            and a.JUMLAH > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                            group by b.KODE
                                            order by TGL
                                            limit 0,1) X on a.KODE = X.KODE
                     left outer join 
                    (select KODE,  sum(PIUTANG) TOTAL_PIUTANG from (
                                        select b.KODE,  sum(jumlah) PIUTANG
                                            from tb_penjualan a, tb_toko b, tb_user_toko c
                                            where a.K_PLG = b.KODE 
                                            and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                            group by b.KODE
                                            union all
                                        select b.KODE,  sum(a.jumlah) - sum(x.TOTAL) PIUTANG
                                            from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
                                            where a.K_PLG = b.KODE 
                                            and a.JUMLAH > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                            and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                            group by b.KODE) x
                                        group by KODE) Y on a.KODE = Y.KODE                      
                    where c.ID_USER=a.KODE
                    and binary c.ID_USER='$KODE'
                and binary c.user_regid ='$user_regid'";
        
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    // public function getPiutangs($KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
    //     $sqladd     = "";
        
    //     if($start == ''){
    //         $start = 0;
    //     }
    //     if($limit == ''){
    //         $limit = maksimalRequest;
    //     }

    //     if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
    //         $sqladd .= " and tgl_jatuh_tempo BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
    //     }

    //     $sql = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO from (
    //             select b.KODE, b.NAMA, b.ALAMAT, sum(total) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
    //             from tb_order a, tb_toko b
    //             where a.KODE_TOKO = b.KODE
    //             group by b.KODE
    //             union all
    //             select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
    //             from tb_pembayaran_toko a, tb_toko b
    //             where a.PELANGGAN = b.KODE
    //             group by b.KODE
    //             ) x
    //             group by NAMA
    //             having KODE = " . $this->db->escape($KODE) . " 
    //             order by TGL_JATUH_TEMPO desc
    //             limit $start , $limit";

    //     $data = $this->db->query($sql);
    //     if($data->num_rows()>=0){
    //         return $data->result();
    //     }else{
    //         return false;
    //     }
    // }
    // public function getDetailPiutang($KODE){
    //     //b.NAMA, b.ALAMAT,
    //     $sql = "select b.KODE,  a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, TOTAL, date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
    //             from tb_order a, tb_toko b
    //             where a.KODE_TOKO = b.KODE
    //             and a.no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.no_faktur = x.no_faktur)
    //             and b.KODE = " . $this->db->escape($KODE) . "
    //             order by TGL_JATUH_TEMPO desc
    //             ";

    //     $data = $this->db->query($sql);
    //     if($data->num_rows()>=0){
    //         return $data->result();
    //     }else{
    //         return false;
    //     }
    // }
    // ------------------------------------------------------------dari Mas PII-----------------------------------------
    public function getPiutangPlg($KODE, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = 25;
        }

        $sql = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, 
                (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, 
                case when sum(tagihan) - sum(pembayaran) > 0 then date_format(TGL_JATUH_TEMPO, '%d-%m-%Y')
                else '-' end TGL_JATUH_TEMPO 
                from (
                select b.KODE, b.NAMA, b.ALAMAT, sum(jumlah) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
                from tb_penjualan a, tb_toko b
                where a.K_PLG = b.KODE 
                group by b.KODE
                union all
                select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
                from tb_pembayaran_toko a, tb_toko b
                where a.PELANGGAN = b.KODE
                group by b.KODE
                ) x
                group by NAMA
                having KODE = " . $this->db->escape($KODE) . "
                order by TGL_JATUH_TEMPO desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    
    public function getDetailPiutang($KODE){
//        $sql = "select KODE, NO_FAKTUR, TGL_FAKTUR, TOTAL, PIUTANG, TGL_JATUH_TEMPO from (
//                select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, (a.total - sum(x.TOTAL)) PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.NO_FAKTUR = z.NO_FAKTUR)
//                 union all
//                 select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, a.total PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                 ) x 
//                     where KODE = " . $this->db->escape($KODE) . "";
        
        
        $sql = "select KODE, NO_FAKTUR, TGL_FAKTUR, TOTAL, PIUTANG, TGL_JATUH_TEMPO from (
                select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, (a.jumlah - sum(x.TOTAL)) PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE 
                        and a.JUMLAH > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.FAKTUR = z.NO_FAKTUR)
                 union all
                 select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, a.jumlah PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                 ) x 
                     where KODE = " . $this->db->escape($KODE) . "";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getPoinPelanggan($KODE){
        //untuk menambahkan custom query
        $sql = "select b.KODE, NAMA, ALAMAT, count(*) JUMLAH_ORDER, sum(JUMLAH) JUMLAH_BELI, sum(a.poin) TOTAL_POIN, 
                (select ifnull(sum(c.POIN),0) from tb_tukar_poin c where c.id_toko = b.kode) TUKAR,
                sum(a.poin) - (select ifnull(sum(c.POIN),0) from tb_tukar_poin c where c.id_toko = b.kode) POIN
                    from  tb_poin_toko a, tb_toko b
                    where b.KODE = a.ID_TOKO
                 group by a.ID_TOKO 
                having b.KODE = " . $this->db->escape($KODE) . "
                order by POIN desc";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    
    public function getDetailPoin($KODE){
        $sql = "select a.NAMA, a.ALAMAT, b.NO_FAKTUR, date_format(b.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, 
                 sum(c.JUMLAH) JUMLAH,  POIN
                from tb_toko a, tb_order b, tb_order_detail c, 
                      tb_produk d, tb_poin_toko e
                where a.KODE = b.KODE_TOKO and b.ID_ORDER = c.ID_ORDER  and is_approval = 1
                and c.ID_PRODUK = d.ID_PRODUK and b.ID_ORDER = e.ID_ORDER
                and a.KODE = " . $this->db->escape($KODE) . "
                group by a.KODE, b.NO_FAKTUR
                order by a.KODE,  date_format(b.TGL_FAKTUR, '%Y-%m-%d') desc
                limit 0,30";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    
    public function getDetailTukar($KODE){
        $sql = "select a.NAMA, a.ALAMAT, date_format(b.TGL_TUKAR, '%d-%m-%Y') TGL_TUKAR, POIN, REWARD
                from tb_toko a, tb_tukar_poin b
                where a.KODE = b.ID_TOKO
                and a.KODE = " . $this->db->escape($KODE) . "
                group by a.KODE, b.TGL_TUKAR
                order by a.KODE,  date_format(b.TGL_TUKAR, '%Y-%m-%d') desc
                limit 0,30";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    // ------------------------------------------------------------dari Mas PII-----------------------------------------
    public function totalNotifHariIni($tanggal_id){
        $sql    ="select ifnull(count(*), 0) as total from tb_notif_pembayaran where ID_NOTIF like '$tanggal_id%'";

        $result = $this->db->query($sql)->result();

        return $result[0]->total;
    }
    public function simpanNotifPembayaran($data_notif){
        $this->db->insert('tb_notif_pembayaran', $data_notif);
    }
    public function getTokoBy($kode){
        $this->db->select('*');
        $this->db->from('tb_toko');
        $this->db->join('tb_user_toko', 'tb_user_toko.id_user = tb_toko.kode');
        $this->db->where('KODE', $kode);
        $this->db->limit(1);

        $result = $this->db->get()->result();
        return $result;
    }
    // ---------------------------------------------untuk notifikasi kolektor-----------------------------------------------
    public function getDaftarPelunasan($id_user, $kode){       //fungsi ini juga ada di Apitokomodel
        $sqladd     = "";
        
//        $sql = "select date_format(y.TGL_NOTIF, '%d-%m-%Y') TGL_NOTIF, z.NAMA KOLEKTOR, x.NAMA, x.NO_FAKTUR, x.TGL_FAKTUR, PIUTANG PIUTANG, TGL_JATUH_TEMPO, y.TOTAL PEMBAYARAN,  date_format(y.TGL_PEMBAYARAN, '%d-%m-%Y') TGL_PEMBAYARAN,  
//                y.ID_NOTIF, x.KODE from (
//                select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, (a.total - sum(x.TOTAL)) PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.NO_FAKTUR = z.NO_FAKTUR)
//                 union all
//                 select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, a.total PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                 ) x, tb_notif_pembayaran y, tb_pegawai z 
//                     where x.KODE = y.KODE_TOKO and y.KOLEKTOR = z.NIK
//                     and y.is_confirm = 1 and y.kolektor = '$id_user'
//                     and x.KODE='$kode'
//                     order by date_format(y.TGL_PEMBAYARAN, '%Y-%m-%d')
//                limit 0 , 1";
        
        $sql = "select date_format(y.TGL_NOTIF, '%d-%m-%Y') TGL_NOTIF, z.NAMA KOLEKTOR, x.NAMA, x.NO_FAKTUR, x.TGL_FAKTUR, PIUTANG PIUTANG, TGL_JATUH_TEMPO, y.TOTAL PEMBAYARAN,  date_format(y.TGL_PEMBAYARAN, '%d-%m-%Y') TGL_PEMBAYARAN,  
                y.ID_NOTIF, x.KODE from (
                select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, (a.jumlah - sum(x.TOTAL)) PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE 
                        and a.JUMLAH > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.FAKTUR = z.NO_FAKTUR)
                 union all
                 select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, a.jumlah PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                 ) x, tb_notif_pembayaran y, tb_pegawai z 
                     where x.KODE = y.KODE_TOKO and y.KOLEKTOR = z.NIK
                     and y.is_confirm = 1 and y.kolektor = '$id_user'
                     and x.KODE='$kode'
                     order by date_format(y.TGL_PEMBAYARAN, '%Y-%m-%d')
                limit 0 , 1";
                
        $data = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function getKoletor($KODE){
        $stat = "select k.ID_USER, k.USER_TOKEN from tb_user k, tb_toko t WHERE k.ID_USER=t.KDKOLEKTOR and t.KODE='$KODE'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return array();
        }
    }
    // -------------------------------------tutup untuk notifikasi kolektor-----------------------------------------------
}