<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Apitoko extends MY_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->model('Apitokomodel');
        //$this->load->model('Test2');
    }
    public function tesfcm(){
        // $registatoin_ids = array(
        //     'deH2yccZx7U:APA91bEhKsoIyd9Po5JRUk4DCip6Q-rn-x8uIE2OdvtqesZSe_kAXUOnwHGoBDmYaExevKSQRkkVa5NZQN--LUBSVRU9M8rizS1G9YygmsKTCQ0G_vWKq-UxmBA4L5GkvdsD6UT-YNV7'
        //     );

        // $message = $this->Apitokomodel->getPembayaran('0001024100');
        // $msg = array(
        //  'body' => $message,
        //  'title' => "PUSH NOTIFICATION"
        //  );
        // // $message = array(
        // //         'status' => 1,
        // //         'data'  => 'data'
        // //     );

        // // 0001024100
        // $this->sendNotification($registatoin_ids, $msg);

        // $ip_server  = 'localhost';
        // $kode       = '0001024100';     //kode pelanggan

        // $url = 'http://'.$ip_server.'/apibig/apitoko/kirimNotifFCM/'.$kode;
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url); //set url
        // curl_setopt($ch, CURLOPT_HEADER, true); //get header
        // curl_setopt($ch, CURLOPT_NOBODY, true); //do not include response body
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //do not show in browser the response
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //follow any redirects
        // curl_exec($ch);
        // $new_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); //extract the url from the header response
        // curl_close($ch);

    }
    public function kirimNotifFCM(){
        $kode =  $this->uri->segment(3);
        if($kode!=''){
            $message = $this->Apitokomodel->getPembayaran($kode);

            $msg = array(
                 'body' => $message,
                 'tipe' => 1,           //tipe 1 untuk notifikasi ketika ada pembayaran
                 'title' => "PUSH NOTIFICATION"
            );

            $registatoin_ids = array(
                $message->USER_REGID
                );
            $this->sendNotification($registatoin_ids, $msg);   
        }
    }
    public function kirimNotifUlangTahun(){
        $kode =  $this->uri->segment(3);
        if($kode!=''){
            $message = $this->Apitokomodel->getTokoBy($kode);

            if(count($message) == 1){
                //kirim notif
                //var_dump($message[0]);
                if($message[0]->tgl_ulang_tahun != null || $message[0]->tgl_ulang_tahun != ''){
                    //kirim notif
                    //MENGHITUNG USIA
                    $date = date('Y-m-d');

                    $a = new DateTime($date);
                    $b = new DateTime($message[0]->tgl_ulang_tahun);
                    $usia = $b->diff($a)->y;

                    $data = array(
                        'KODE'      => $message[0]->KODE,
                        'NAMA'      => $message[0]->NAMA,
                        'tgl_ulang_tahun'      => $message[0]->tgl_ulang_tahun,
                        'USIA'      => $usia,
                        'message'   => 'Selamat kepada '.$message[0]->NAMA.' atas hari jadi ke-'.$usia.'.',
                    );
                    $msg = array(
                         'body' => $data,
                         'tipe' => 2,           //tipe 2 untuk notifikasi ulang tahun
                         'title' => "PUSH NOTIFICATION"
                    );
                    $registatoin_ids = array(
                        $message[0]->USER_REGID
                        );
                    $this->sendNotification($registatoin_ids, $msg);   
                    //var_dump($msg);
                }else{
                    echo "data ulang tahun tidak ada.";
                }
            }else{

            }
        }
    }
    public function login()
    {
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_name)     ? $user_name        = $data->user_name      : $user_name     = '';
            isset($data->user_password) ? $user_password    = $data->user_password  : $user_password = '';

            isset($data->user_regid)    ? $user_regid       = $data->user_regid     : $user_regid    = '';
        
            if($user_name != '' && $user_password != '' && $user_regid != ''){
                $userlogin = $this->Apitokomodel->login($user_name, $user_password);
                if($userlogin){
                    //megnupdate token user
                    if($this->Apitokomodel->updateToken($user_name, $user_password, $user_regid)){
                        //mencetak data user
                        $data_user  = $this->Apitokomodel->getuser($user_name, $user_password);
                        $status     = 1;
                        $message    = "Login success.";
                        $data       = $data_user[0];
                    }else{
                        $status     = 0;
                        $message    = "Tidak dapat melakukan input data.";
                        $data       = null;
                    }
                }else{
                    $message    = 'Username dan password tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field tidak boleh kosong.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }

    public function logout(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';

            $userlogout     = $this->Apitokomodel->logout($id_user, $user_regid);

            if($userlogout){
                $message    = 'Logout berhasil.';
                $data       = 'null';
                $status     = 1;
            }else{
                $message    = 'Logout gagal.';
                $data       = null;
                $status     = 1;
            }

            $response   = array(
                    'status'    => $status,
                    'message'     => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getPembayaran()
    {
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';

            isset($data->TGL_AWAL)      ? $TGL_AWAL     = $data->TGL_AWAL       : $TGL_AWAL     = '';
            isset($data->TGL_AKHIR)     ? $TGL_AKHIR    = $data->TGL_AKHIR      : $TGL_AKHIR    = '';
            isset($data->START)         ? $START        = $data->START          : $START        = '';
            isset($data->LIMIT)         ? $LIMIT        = $data->LIMIT          : $LIMIT        = '';


            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_pembayaran = $this->Apitokomodel->getPembayarans($KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if($data_pembayaran){
                        $pembayaran  = array(
                                "total"      => count($data_pembayaran),
                                "pembayaran" => $data_pembayaran
                            );
                        $message    = 'Data pembayaran.';
                        $data       = $pembayaran;
                        $status     = 1;
                    }else{
                        $message    = 'Data pembayaran tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getKunjungan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            isset($data->TGL_AWAL)      ? $TGL_AWAL     = $data->TGL_AWAL       : $TGL_AWAL     = '';
            isset($data->TGL_AKHIR)     ? $TGL_AKHIR    = $data->TGL_AKHIR      : $TGL_AKHIR    = '';
            isset($data->START)         ? $START        = $data->START          : $START        = '';
            isset($data->LIMIT)         ? $LIMIT        = $data->LIMIT          : $LIMIT        = '';

            // echo $KODE. "   ".$user_regid;
            // exit();
            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_kunjungan = $this->Apitokomodel->getKunjungans($KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if($data_kunjungan){
                        $kunjungan  = array(
                                "total"     => count($data_kunjungan),
                                "kunjungan" => $data_kunjungan
                            );
                        $message    = 'Data kunjungan.';
                        $data       = $kunjungan;
                        $status     = 1;
                    }else{
                        $message    = 'Data kunjungan tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }

    public function getOrder(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));

            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            isset($data->TGL_AWAL)      ? $TGL_AWAL     = $data->TGL_AWAL       : $TGL_AWAL     = '';
            isset($data->TGL_AKHIR)     ? $TGL_AKHIR    = $data->TGL_AKHIR      : $TGL_AKHIR    = '';
            isset($data->START)         ? $START        = $data->START          : $START        = '';
            isset($data->LIMIT)         ? $LIMIT        = $data->LIMIT          : $LIMIT        = '';


            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_kunjungan = $this->Apitokomodel->getOrders($KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if($data_kunjungan){
                        $kunjungan  = array(
                                "total"     => count($data_kunjungan),
                                "order"     => $data_kunjungan
                            );
                        $message    = 'Data order.';
                        $data       = $kunjungan;
                        $status     = 1;
                    }else{
                        $message    = 'Data order tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getDetailOrder(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            isset($data->ID_ORDER)      ? $ID_ORDER     = $data->ID_ORDER       : $ID_ORDER     = '';

            if($KODE != '' && $user_regid != '' && $ID_ORDER != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_order = $this->Apitokomodel->getDetailOrders($ID_ORDER);
                    if($data_order){
                        $order  = array(
                                "total"     => count($data_order),
                                "order"     => $data_order
                            );
                        $message    = 'Data detail order.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail order tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getDataPelanggan()
    {
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';

            // echo $KODE. "   ".$user_regid;
            // exit();
            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_user  = $this->Apitokomodel->getDataPelanggan($KODE, $user_regid);
                    if($data_user){
                        $status     = 1;
                        $message    = "Data Pelanggan";
                        $data       = $data_user[0];
                    }else{
                        $message    = 'Data Pelanggan tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    // public function getPiutang(){
    //     if($this->checkApp($this->input->get_request_header('appToken'))){
    //         $status     = -1;
    //         $message    = '';
    //         $data       = '';

    //         $data = (object)json_decode(file_get_contents('php://input'));
            
    //         isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
    //         isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
    //         isset($data->TGL_AWAL)      ? $TGL_AWAL     = $data->TGL_AWAL       : $TGL_AWAL     = '';
    //         isset($data->TGL_AKHIR)     ? $TGL_AKHIR    = $data->TGL_AKHIR      : $TGL_AKHIR    = '';
    //         isset($data->START)         ? $START        = $data->START          : $START        = '';
    //         isset($data->LIMIT)         ? $LIMIT        = $data->LIMIT          : $LIMIT        = '';

    //         if($KODE != '' && $user_regid != ''){
    //             //mengecek login user
    //             $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
    //             if($is_login){
    //                 $data_piutang = $this->Apitokomodel->getPiutangs($KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
    //                 // var_dump($data_kunjungan);
    //                 // exit();
    //                 if($data_piutang){
    //                     $kunjungan  = array(
    //                             "total"     => count($data_piutang),
    //                             "piutang"     => $data_piutang
    //                         );
    //                     $message    = 'Data piutang.';
    //                     $data       = $kunjungan;
    //                     $status     = 1;
    //                 }else{
    //                     $message    = 'Data piutang tidak ditemukan.';
    //                     $data       = null;
    //                     $status     = 0;
    //                 }
    //             }else{
    //                 $message    = 'Akun tidak ditemukan.';
    //                 $data       = null;
    //                 $status     = 0;
    //             }
    //         }else{
    //             $message    = 'Field is empty.';
    //             $data       = null;
    //             $status     = 0;
    //         }
    //         $response   = array(
    //                 'status'    => $status,
    //                 'message'   => $message,
    //                 'data'      => $data
    //             );
    //         $this->makeOutput($response);
    //     }else{
    //         $this->jsonNoRespon();
    //     }
    // }
    // public function getDetailPiutang(){
    //     if($this->checkApp($this->input->get_request_header('appToken'))){
    //         $status     = -1;
    //         $message    = '';
    //         $data       = '';

    //         $data = (object)json_decode(file_get_contents('php://input'));
            
    //         isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
    //         isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
    //         isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';

    //         if($KODE != '' && $user_regid != ''){
    //             //mengecek login user
    //             $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
    //             if($is_login){
    //                 $data_piutang = $this->Apitokomodel->getDetailPiutang($KODE);
    //                 if($data_piutang){
    //                     $order  = array(
    //                             "total"     => count($data_piutang),
    //                             "piutang"   => $data_piutang
    //                         );
    //                     $message    = 'Data detail Piutang.';
    //                     $data       = $order;
    //                     $status     = 1;
    //                 }else{
    //                     $message    = 'Data detail piutang tidak ditemukan.';
    //                     $data       = null;
    //                     $status     = 0;
    //                 }
    //             }else{
    //                 $message    = 'Akun tidak ditemukan.';
    //                 $data       = null;
    //                 $status     = 0;
    //             }
    //         }else{
    //             $message    = 'Field is empty.';
    //             $data       = null;
    //             $status     = 0;
    //         }
    //         $response   = array(
    //                 'status'    => $status,
    //                 'message'   => $message,
    //                 'data'      => $data
    //             );
    //         $this->makeOutput($response);
    //     }else{
    //         $this->jsonNoRespon();
    //     }
    // }

    // --------------------------------------------------------dari Mas Pii-----------------------------------------
    public function getPiutang(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            isset($data->START)         ? $START        = $data->START          : $START        = '';
            isset($data->LIMIT)         ? $LIMIT        = $data->LIMIT          : $LIMIT        = '';

            // echo $KODE. "   ".$user_regid;
            // exit();
            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_piutang = $this->Apitokomodel->getPiutangPlg($KODE, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if($data_piutang){
                        $piutang  = array(
                                "total"     => count($data_piutang),
                                "piutang" => $data_piutang
                            );
                        $message    = 'Data Piutang.';
                        $data       = $piutang;
                        $status     = 1;
                    }else{
                        $message    = 'Data Piutang tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    
    public function getDetailPiutang(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            

            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_piutang = $this->Apitokomodel->getDetailPiutang($KODE);
                    if($data_piutang){
                        $order  = array(
                                "total"     => count($data_piutang),
                                "piutang"   => $data_piutang
                            );
                        $message    = 'Data detail Piutang.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail piutang tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getPoin(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';

            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_poin = $this->Apitokomodel->getPoinPelanggan($KODE);
                    // var_dump($data_kunjungan);
                    // exit();
                    if($data_poin){
                        $message    = 'Data Poin Pelanggan.';
                        $data       = $data_poin;
                        $status     = 1;
                    }else{
                        $message    = 'Data poin pelanggan tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    
    public function getDetailPoin(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)      ? $KODE     = $data->KODE       : $KODE    = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';

            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_poin = $this->Apitokomodel->getDetailPoin($KODE);
                    if($data_poin){
                        $order  = array(
                                "total"     => count($data_poin),
                                "poin"     => $data_poin
                            );
                        $message    = 'Data detail poin pelanggan.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail poin tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    
    public function getDetailTukar(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)      ? $KODE     = $data->KODE       : $KODE    = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';

            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_tukar = $this->Apitokomodel->getDetailTukar($KODE);
                    if($data_tukar){
                        $order  = array(
                                "total"     => count($data_tukar),
                                "tukar"     => $data_tukar
                            );
                        $message    = 'Data detail tukar poin pelanggan.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail tukar poin tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    // --------------------------------------------------------dari Mas Pii-----------------------------------------

    public function getDetailPelunasan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            

            if($KODE != '' && $user_regid != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    $data_piutang = $this->Apitokomodel->getDetailPiutang($KODE);
                    if($data_piutang){
                        $order  = array(
                                "total"     => count($data_piutang),
                                "piutang"   => $data_piutang
                            );
                        $message    = 'Data detail Piutang.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail piutang tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function bayarPelunasan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->KDSALES)       ? $KDSALES      = $data->KDSALES        : $KDSALES      = '';
            isset($data->user_regid)    ? $user_regid   = $data->user_regid     : $user_regid   = '';
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->jumlah_bayar)  ? $jumlah_bayar = $data->jumlah_bayar   : $jumlah_bayar = '';
            isset($data->tgl_bayar)     ? $tgl_bayar    = $data->tgl_bayar      : $tgl_bayar    = '';
            isset($data->faktur_bayar)  ? $faktur_bayar = $data->faktur_bayar   : $faktur_bayar = '';

            if($KDSALES != '' && $user_regid != '' && $KODE != '' && $jumlah_bayar != '' && $tgl_bayar != '' && $faktur_bayar != ''){
                //mengecek login user
                $is_login   = $this->Apitokomodel->getUserByToken($KODE, $user_regid);
                if($is_login){
                    //untuk membuat id notif
                    $tanggal    = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $notif_ke   = ($this->Apitokomodel->totalNotifHariIni($tanggal_id)+1);

                    if($notif_ke<10){
                        $id_notif   = $tanggal_id.'000'.$notif_ke;
                    }else if($notif_ke>=10 && $notif_ke<100){
                        $id_notif   = $tanggal_id.'00'.$notif_ke;
                    }else if($notif_ke>=100 && $notif_ke<1000){
                        $id_notif   = $tanggal_id.'0'.$notif_ke;
                    }else{
                        $id_notif   = $tanggal_id.''.$notif_ke;
                    }

                    $kolektor   = $this->Apitokomodel->getKoletor($KODE);
                    $id_kolektor= $kolektor->ID_USER;

                    $data_notif = array(
                        'ID_NOTIF'      => $id_notif,
                        'TGL_NOTIF'     => $tanggal,
                        'NIK'           => $KDSALES,
                        'KODE_TOKO'     => $KODE,
                        'NO_FAKTUR'     => $faktur_bayar,
                        'TOTAL'         => $jumlah_bayar,
                        'TGL_PEMBAYARAN'=> $tgl_bayar,
                        'is_confirm'    => 1,
                        'KOLEKTOR'      => $id_kolektor,
                    );

                    //menyimpan notif ke database
                    $this->Apitokomodel->simpanNotifPembayaran($data_notif);

                    //mengirim notifikasi ke kolektor
                    $message    = $this->Apitokomodel->getDaftarPelunasan($id_kolektor, $KODE);

                    $msg = array(
                         'body' => $message,
                         'tipe' => 1,           //tipe 1 untuk notifikasi ketika ada pembayaran
                         'title' => "PUSH NOTIFICATION"
                    );

                    $registatoin_ids = array(
                        $kolektor->USER_TOKEN
                        );
                    if($message != null & $kolektor != null){
                        $this->sendNotificationKolektor($registatoin_ids, $msg);
                    }

                    $message    = 'Permintaan pembayaran berhasil disimpan.';
                    $data       = null;
                    $status     = 1;
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
}