<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apikolektormodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getUserByToken($id_user, $user_token){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where id_user = '$id_user' and binary user_token = '$user_token' and id_role=5";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }   

    public function login($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and binary password = '$password' and id_role=5";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function updateToken($username, $password, $token){
        $query = "update tb_user set user_token='$token' 
                    where binary user_name='$username' and binary password = '$password' and id_role=5";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function getUser($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and binary password = '$password' and id_role=5";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }

    public  function logout($id_user, $user_token){
        $sql    = "select * from tb_user where id_user='".$id_user."' or user_token='".$user_token."' and id_role=5";
        $data   = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            $data = $data[0];

            if($this->updateToken($data->USER_NAME, $data->PASSWORD, '')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    
    public function getTotalPiutang($KODE){
//        $stat = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO from (
//            select b.KODE, b.NAMA, b.ALAMAT, sum(total) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
//            from tb_order a, tb_toko b
//            where a.KODE_TOKO = b.KODE and is_approval = 1
//            group by b.KODE
//            union all
//            select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
//            from tb_pembayaran_toko a, tb_toko b
//            where a.PELANGGAN = b.KODE
//            group by b.KODE
//            ) x
//            group by NAMA
//            having KODE = '$KODE'
//            order by TGL_JATUH_TEMPO desc";
        
        $stat = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO from (
            select b.KODE, b.NAMA, b.ALAMAT, sum(jumlah) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
            from tb_penjualan a, tb_toko b
            where a.K_PLG = b.KODE
            group by b.KODE
            union all
            select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
            from tb_pembayaran_toko a, tb_toko b
            where a.PELANGGAN = b.KODE
            group by b.KODE
            ) x
            group by NAMA
            having KODE = '$KODE'
            order by TGL_JATUH_TEMPO desc";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->PIUTANG;
        }else{
            return 0;
        }
    }
    public function getJatuhTempo($KODE){
        $stat = "SELECT a.JATUHTEMPOBAYAR
                FROM tb_toko a
                where a.KODE='$KODE'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->JATUHTEMPOBAYAR;
        }else{
            return 0;
        }
    }
   
    public function getDefaultGudang($id_user){
        $stat = "SELECT a.UNIT
                FROM tb_pegawai a
                where a.nik='$id_user'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->UNIT;
        }else{
            return "";
        }
    }
    
    public function getPiutangs($id_user, $KODE, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " having KODE = " . $this->db->escape($KODE) . " ";
        }

//        $sql = "select KODE, NAMA, ALAMAT, sum(PIUTANG) PIUTANG, sum(JUMLAH_FAKTUR) JUMLAH_FAKTUR, min(TGL_JATUH_TEMPO) TGL_JATUH_TEMPO from (
//                    select b.KODE, b.NAMA, b.ALAMAT, sum(total) PIUTANG, count(*) JUMLAH_FAKTUR, 
//                        date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                        and b.KDKOLEKTOR = '$id_user' 
//                        group by b.KODE
//                        union all
//                    select b.KODE, b.NAMA, b.ALAMAT, sum(a.total) - sum(x.TOTAL) PIUTANG,  count(*) JUMLAH_FAKTUR, 
//                        date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                        and b.KDKOLEKTOR = '$id_user' 
//                        group by b.KODE) x
//                    group by KODE
//                    " . $sqladd . "
//                    order by TGL asc";
        
        $sql = "select KODE, NAMA, ALAMAT, sum(PIUTANG) PIUTANG, sum(JUMLAH_FAKTUR) JUMLAH_FAKTUR, min(TGL_JATUH_TEMPO) TGL_JATUH_TEMPO from (
                    select b.KODE, b.NAMA, b.ALAMAT, sum(jumlah) PIUTANG, count(*) JUMLAH_FAKTUR, 
                        date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                        and b.KDKOLEKTOR = '$id_user' 
                        group by b.KODE
                        union all
                    select b.KODE, b.NAMA, b.ALAMAT, sum(a.jumlah) - sum(x.TOTAL) PIUTANG,  count(*) JUMLAH_FAKTUR, 
                        date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE
                        and a.JUMLAH > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                        and b.KDKOLEKTOR = '$id_user' 
                        group by b.KODE) x
                    group by KODE
                    " . $sqladd . "
                    order by TGL asc";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getDetailPiutang($KODE){
        //b.NAMA, b.ALAMAT,
//        $sql = "select KODE, NO_FAKTUR, TGL_FAKTUR, TOTAL, PIUTANG, TGL_JATUH_TEMPO from (
//                select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, (a.total - sum(x.TOTAL)) PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.NO_FAKTUR = z.NO_FAKTUR)
//                 union all
//                 select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, a.total PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                 ) x 
//                     where KODE = " . $this->db->escape($KODE) . "";
        
         $sql = "select KODE, NO_FAKTUR, TGL_FAKTUR, TOTAL, PIUTANG, TGL_JATUH_TEMPO from (
                select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, (a.jumlah - sum(x.TOTAL)) PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE 
                        and a.JUMLAH > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.FAKTUR = z.NO_FAKTUR)
                 union all
                 select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, a.jumlah PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                 ) x 
                     where KODE = " . $this->db->escape($KODE) . "";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    
    public function getDaftarPelunasan($id_user,  $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and y.TGL_PEMBAYARAN BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

//        $sql = "select date_format(y.TGL_NOTIF, '%d-%m-%Y') TGL_NOTIF, z.NAMA KOLEKTOR, x.NAMA, x.NO_FAKTUR, x.TGL_FAKTUR, PIUTANG PIUTANG, TGL_JATUH_TEMPO, y.TOTAL PEMBAYARAN,  date_format(y.TGL_PEMBAYARAN, '%d-%m-%Y') TGL_PEMBAYARAN,  
//                y.ID_NOTIF, x.KODE from (
//                select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, (a.total - sum(x.TOTAL)) PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.NO_FAKTUR = z.NO_FAKTUR)
//                 union all
//                 select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, a.total PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                 ) x, tb_notif_pembayaran y, tb_pegawai z 
//                     where x.KODE = y.KODE_TOKO and y.KOLEKTOR = z.NIK
//                     and y.is_confirm = 1 and y.kolektor = '$id_user'
//                " . $sqladd . "
//                     order by date_format(y.TGL_PEMBAYARAN, '%Y-%m-%d')
//                limit $start , $limit ";
        
        $sql = "select date_format(y.TGL_NOTIF, '%d-%m-%Y') TGL_NOTIF, z.NAMA KOLEKTOR, x.NAMA, x.NO_FAKTUR, x.TGL_FAKTUR, PIUTANG PIUTANG, TGL_JATUH_TEMPO, y.TOTAL PEMBAYARAN,  date_format(y.TGL_PEMBAYARAN, '%d-%m-%Y') TGL_PEMBAYARAN,  
                y.ID_NOTIF, x.KODE from (
                select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, (a.jumlah - sum(x.TOTAL)) PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE
                        and a.JUMLAH > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.FAKTUR = z.NO_FAKTUR)
                 union all
                 select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, a.jumlah PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                 ) x, tb_notif_pembayaran y, tb_pegawai z 
                     where x.KODE = y.KODE_TOKO and y.KOLEKTOR = z.NIK
                     and y.is_confirm = 1 and y.kolektor = '$id_user'
                limit $start , $limit ";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getToko($id_user, $start, $limit){
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = 25;
        }
        $sql = "select a.KODE, a.NAMA, a.ALAMAT
                from tb_toko a
                where a.KDKOLEKTOR='$id_user'
                limit $start, $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
   
}