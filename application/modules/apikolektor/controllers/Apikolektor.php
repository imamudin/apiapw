<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apikolektor extends MY_Controller {

    //method checkApp terletak di MY_Controller

    function __construct() {
        parent::__construct();
        $this->load->model('Apikolektormodel');
    }
    public function kirimNotifFCM(){
        $kode =  $this->uri->segment(3);
        if($kode!=''){
            $message = $this->Apikolektormodel->getTagihan($kode);

            $msg = array(
                 'body' => $message,
                 'tipe' => 1,           //tipe 1 untuk notifikasi ketika ada pembayaran
                 'title' => "PUSH NOTIFICATION"
            );

            $registatoin_ids = array(
                $message->USER_REGID
                );
            $this->sendNotification($registatoin_ids, $msg);   
        }
    }
    public function login()
    {
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_name)     ? $user_name     = $data->user_name      : $user_name     = '';
            isset($data->user_password) ? $user_password = $data->user_password  : $user_password = '';

            isset($data->user_regid)    ? $user_regid       = $data->user_regid     : $user_regid    = '';
            
            if($user_name != '' && $user_password != '' && $user_regid != ''){
                $userlogin = $this->Apikolektormodel->login($user_name, $user_password);
                if($userlogin){
                    //megnupdate token user
                    if($this->Apikolektormodel->updateToken($user_name, $user_password, $user_regid)){
                        //mencetak data user
                        $data_user  = $this->Apikolektormodel->getuser($user_name, $user_password);
                        $status     = 1;
                        $message    = "Login success.";
                        $data       = $data_user[0];
                    }else{
                        $status     = 0;
                        $message    = "Tidak dapat melakukan input data.";
                        $data       = null;
                    }
                }else{
                    $message    = 'Username dan password tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field tidak boleh kosong.';
                $data       = null;
                $status     = 0;
            }
            
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }

    public function logout(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';

            $userlogout     = $this->Apikolektormodel->logout($id_user, $user_token);

            if($userlogout){
                $message    = 'Logout berhasil.';
                $data       = 'null';
                $status     = 1;
            }else{
                $message    = 'Logout gagal.';
                $data       = null;
                $status     = -1;
            }

            $response   = array(
                    'status'    => $status,
                    'message'     => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }

    public function getPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));
            
            isset($data->id_user) ? $id_user        = $data->id_user    : $id_user = '';
            isset($data->user_token) ? $user_token  = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE   = $data->KODE  : $KODE  = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apikolektormodel->getPiutangs($id_user, $KODE, $START, $LIMIT);
                    if ($data_piutang) {
                        $kunjungan = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data piutang.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apikolektormodel->getDetailPiutang($KODE);
                    if ($data_piutang) {
                        $order = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data detail Piutang.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDaftarPelunasan() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_pelunasan = $this->Apikolektormodel->getDaftarPelunasan($id_user, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_pelunasan) {
                        $pelunasan = array(
                            "total" => count($data_pelunasan),
                            "pelunasan" => $data_pelunasan
                        );
                        $message = 'Data Pembayaran.';
                        $data = $pelunasan;
                        $status = 1;
                    } else {
                        $message = 'Data Pembayaran tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getToko() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                $data_toko = $this->Apikolektormodel->getToko($id_user, $START, $LIMIT);
                if ($data_toko) {
                    $order = array(
                        "total" => count($data_toko),
                        "toko" => $data_toko
                    );
                    $message = 'Data toko.';
                    $data = $order;
                    $status = 1;
                } else {
                    $message = 'Data toko tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
}


