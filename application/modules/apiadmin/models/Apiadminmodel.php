<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apiadminmodel extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
    public function login($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and  binary password = '$password' and id_role=1";	//role 1 untuk admin
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function updateToken($username, $password, $token){
        $query = "update tb_user set user_token='$token' 
                    where binary user_name='$username' and binary password = '$password'";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function getUser($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and binary password = '$password' and id_role=1";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getUserByToken($id_user, $user_token){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary id_user = '$id_user' and binary user_token = '$user_token' and id_role=1";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public  function logout($id_user, $user_token){
        $sql    = "select * from tb_user where id_user='".$id_user."' or user_token='".$user_token."' and id_role=1";	//untuk role admin
        $data   = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            $data = $data[0];

            if($this->updateToken($data->USER_NAME, $data->PASSWORD, '')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function scanToko($KODE){
        $stat = "select a.*, b.NAMA as NAMA_SALES from  tb_toko a
                LEFT join tb_pegawai b on b.NIK=a.KDSALES
                where a.KODE = '$KODE'";
        $data = $this->db->query($stat);
        
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function updatekoordinat($KODE, $LATITUDE, $LONGITUDE){
    	$stat = "update tb_toko set LATITUDE='$LATITUDE' , LONGITUDE='$LONGITUDE' where KODE='$KODE'";
        $data = $this->db->query($stat);
        if($data){
        	return true;
        }else{
            return false;
        }
    }
    public function getToko($search, $start, $limit){
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }
        $sql = "select a.KODE, a.NAMA, a.ALAMAT
                from tb_toko a
                where a.NAMA like '%$search%'
                limit $start, $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
}