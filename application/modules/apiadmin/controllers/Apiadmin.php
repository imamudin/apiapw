<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Apiadmin extends MY_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->model('Apiadminmodel');
        //$this->load->model('Test2');
    }
    public function login()
    {
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_name)     ? $username     = $data->user_name      : $username     = '';
            isset($data->user_password) ? $userpassword = $data->user_password  : $userpassword = '';

            $userlogin = $this->Apiadminmodel->login($username, $userpassword);

            if($userlogin){
                $date = date('YmdHis');
                $token = md5($date);

                //megnupdate token user
                if($this->Apiadminmodel->updateToken($username, $userpassword, $token)){
                    //mencetak data user
                    $data_user  = $this->Apiadminmodel->getuser($username, $userpassword);
                    $status     = 1;
                    $message    = "Login success.";
                    $data       = $data_user[0];
                }else{
                    $status     = 0;
                    $message    = "Tidak dapat melakukan input data.";
                    $data       = null;
                }
            }else{
                $message    = 'Username dan password tidak ditemukan.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }

    public function logout(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';

            $userlogout     = $this->Apiadminmodel->logout($id_user, $user_token);

            if($userlogout){
                $message    = 'Logout berhasil.';
                $data       = 'null';
                $status     = 1;
            }else{
                $message    = 'Logout gagal.';
                $data       = null;
                $status     = -1;
            }

            $response   = array(
                    'status'    => $status,
                    'message'     => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function scantoko()
    {
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';

            if($id_user != '' && $user_token != '' && $KODE != ''){
                //mengecek login user
                $is_login   = $this->Apiadminmodel->getUserByToken($id_user, $user_token);
                if($is_login){
                    $scanToko  = $this->Apiadminmodel->scanToko($KODE);

                    if($scanToko){
                        $newdata    = array(
                            'toko'      => $scanToko,
                        );
                        $message    = "Scan berhasil";
                        $data       = $newdata;
                        $status     = 1;
                    }else{
                        $message    = "Data toko tidak ditemukan.";
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function updatekoordinat(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->LATITUDE)      ? $LATITUDE     = $data->LATITUDE       : $LATITUDE     = '';
            isset($data->LONGITUDE)     ? $LONGITUDE    = $data->LONGITUDE      : $LONGITUDE    = '';

            if($id_user != '' && $user_token != '' && $KODE != '' && $LATITUDE != '' && $LONGITUDE != ''){
                //mengecek login user
                $is_login   = $this->Apiadminmodel->getUserByToken($id_user, $user_token);
                if($is_login){
                    $update  = $this->Apiadminmodel->updatekoordinat($KODE, $LATITUDE, $LONGITUDE);

                    if($update){
                        $message    = "Koordinat berhasil dirubah.";
                        $data       = true;
                        $status     = 1;
                    }else{
                        $message    = "Data toko tidak ditemukan.";
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getToko() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';

            isset($data->search)        ? $search       = $data->search     : $search   = '';
            isset($data->START)         ? $START        = $data->START      : $START    = '';
            isset($data->LIMIT)         ? $LIMIT        = $data->LIMIT      : $LIMIT    = '';

            if ($id_user != '' && $user_token != '') {
                $data_toko = $this->Apiadminmodel->getToko($search, $START, $LIMIT);
                if ($data_toko) {
                    $order = array(
                        "total" => count($data_toko),
                        "toko" => $data_toko
                    );
                    $message = 'Data toko.';
                    $data = $order;
                    $status = 1;
                } else {
                    $message = 'Data toko tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
}